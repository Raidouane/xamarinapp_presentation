﻿using System;
using Xamarin.Forms;

namespace HelloMySelf
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            RenderPresentationLayout();
        }

        protected void GoToLinkedin()
        {
            Device.OpenUri(new Uri("https://www.linkedin.com/in/raidouane-el-moukhtari/"));
        }

        private void RenderPresentationLayout()
        {
            StackLayout layout = new StackLayout
            {
                BackgroundColor = Color.Gold
            };
            FormattedString formattedString = new FormattedString();
            Span helloText = new Span
            {
                Text = "Hello you \ud83d\udc4b ! My name is ",
                ForegroundColor = Color.Black,
            };
            Span nameText = new Span
            {
                Text = "Radouane MOUKHTARI",
                ForegroundColor = Color.Chocolate,
                FontAttributes = FontAttributes.Bold,
            };
            Span presentationText = new Span
            {
                Text = ". I'm a french student in Master degree (4th year) in Computer Sciences who will finishes his Master in 2020. I also working as full-stack developer in remote with some french enterprises. To Know more about me, click ",
                ForegroundColor = Color.Black,
                FontAttributes = FontAttributes.Italic,
            };
            TapGestureRecognizer redirectToLn = new TapGestureRecognizer
            {
                Command = new Command(GoToLinkedin)
            };

            Span lnText = new Span
            {
                Text = "here",
                ForegroundColor = Color.Blue,
                TextDecorations = TextDecorations.Underline,
                FontAttributes = FontAttributes.Bold,
                GestureRecognizers = { redirectToLn }
            };
           
            formattedString.Spans.Add(helloText);
            formattedString.Spans.Add(nameText);
            formattedString.Spans.Add(presentationText);
            formattedString.Spans.Add(lnText);

            Label presentationLabel = new Label
            {
                FormattedText = formattedString,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
            };
            layout.Children.Add(presentationLabel);
            Content = layout;
        }
    }
}
